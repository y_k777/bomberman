﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;

public class BonusDeruSc : MonoBehaviour
{
    public GameObject UnityChan;
    public GameObject hidden_bpanel;
    public GameObject hidden_rokuhachi;

    public AudioSource aud;
    public AudioClip ideyo;

    public float time = 5;

    public int enemyZan;

    public int bpf;

    public int Bomnosousuu;

    public int X68;


    // Start is called before the first frame update
    void Start()
    {
        this.aud = GetComponent<AudioSource>();//各パネル出現方法に使用可（出現座標は中央固定）
        int enemyZan = UnityChan.GetComponent<BonusSc>().enemyZan;
        int Bomnosousuu = UnityChan.GetComponent<PlayerMove>().Bomnosousuu;

        bpf = 0;
        X68 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        int Bomnosousuu = UnityChan.GetComponent<PlayerMove>().Bomnosousuu;//取り込めてない？
        X68K();//このパブリック？メソッドに到達していない？
    }

    public void OnTriggerEnter(Collider other)
    {
        int enemyZan = UnityChan.GetComponent<BonusSc>().enemyZan;
        if (other.tag == "D_close" && enemyZan > 0 && bpf == 0)
        {
            bpf = 1;
            aud.PlayOneShot(this.ideyo);
            Vector3 tmp = GameObject.FindWithTag("B_b").transform.position;
            GameObject.FindWithTag("B_b").transform.position = new Vector3(tmp.x, tmp.y + 2.0f, tmp.z);
        }
    }

    public void X68K()
    {
        if (UnityChan.GetComponent<PlayerMove>().Bomnosousuu > 2 && X68 == 0)
        {
            X68 = 1;
            aud.PlayOneShot(this.ideyo);
            Vector3 tmp1 = GameObject.FindWithTag("B_68").transform.position;
            GameObject.FindWithTag("B_68").transform.position = new Vector3(tmp1.x, tmp1.y + 1.0f, tmp1.z);

        }

    }


}






