﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;

//シングルトン（理解に至らず）

//DontDestroyOnLoadで残したオブジェクトはどのようにすれば破壊や操作できるのでしょうか。

//「DontDestroyOnLoadが付いているだけで、実質的にはただのGameObject」です。
//なので何らかの方法（GameObject.Findするでも最初から変数に入れておくでも）で取得・参照し、
//Destroy(対象のGameObject); で削除、
//対象のGameObject.SetActive(false);でアクティブをオフなど、
//その他通常のGameObjectと同じように扱えます。



public class DontDestroyObjectManager : MonoBehaviour
{
    List<GameObject> dontDestroyObjects = new List<GameObject>();

    public void DontDestroyOnLoad(GameObject obj)
    {
        Object.DontDestroyOnLoad(obj);
        dontDestroyObjects.Add(obj);
    }

    public void DestoryAll()
    {
        foreach (var obj in dontDestroyObjects)
        {
            GameObject.Destroy(obj);
        }
        dontDestroyObjects.Clear();
    }
}