﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Endunity : MonoBehaviour
{
    int x;
    int y;
    int z;
    int w;

    public GameObject unitychan;
    public GameObject dodai;

    public AudioSource audBGM1;
    public AudioSource audSE;
    public AudioSource audBGM2;

    public AudioClip clear;
    public AudioClip maveras;
    public AudioClip kansei;

    // Start is called before the first frame update
    void Start()
    {
        //GameObject.Find("")

        this.audBGM1 = GetComponent<AudioSource>();
        this.audBGM2 = GetComponent<AudioSource>();
        this.audSE = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Hit" + collision.gameObject.name);
        if (collision.gameObject.name == "Ending_bg")
        {
            Chakuchi();
        }
    }

    public void Chakuchi()
    {
        GameObject.FindWithTag("Endtext").transform.rotation = new Quaternion(x, y, z, w);
        audBGM1.PlayOneShot(this.clear);
        audBGM2.PlayOneShot(this.maveras);
        audSE.PlayOneShot(this.kansei);
    }

}
