﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pd_dw : MonoBehaviour

{
    public float speeddash;
    bool push = false;


    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("unitychan").GetComponent<PlayerMove>();
        speeddash = 1.0f;
    }

    public void PushDown()
    {
        push = true;
    }

    public void PushUp()
    {
        push = false;
    }

    private void Update()
    {
        if (push)
        {
            UpClick();
        }
    }

    public void UpClick()

    {
        speeddash = GameObject.Find("unitychan").GetComponent<PlayerMove>().speeddash;
        GameObject.Find("unitychan").transform.Translate(transform.forward * -5.0f * speeddash * Time.deltaTime);
    }
}
