﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInputManager : MonoBehaviour
{
    public static UnityAction OnUpArrow;
    public static UnityAction OnDownArrow;
    public static UnityAction OnRightArrow;
    public static UnityAction OnLeftArrow;
    public static UnityAction OnSpaceKeyDown;


    void Update()
    {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            OnUpArrow.Invoke();
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            OnDownArrow.Invoke();
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            OnRightArrow.Invoke();
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            OnLeftArrow.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSpaceKeyDown.Invoke();
        }
    }
}
