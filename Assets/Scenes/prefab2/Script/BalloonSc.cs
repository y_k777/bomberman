﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class BalloonSc : MonoBehaviour
{
    public NavMeshAgent Balloon;
    public GameObject Bomberman;
    public GameObject Explsionpre;

    public GameObject Canvas;
    public GameObject Score;

    bool isLose = false;


    // Start is called before the first frame update
    void Start()
    {
        Balloon = gameObject.GetComponent<NavMeshAgent>();


        Bomberman = GameObject.FindGameObjectWithTag("Player");
        Canvas = GameObject.FindGameObjectWithTag("CanvasT");
        Score = GameObject.FindGameObjectWithTag("ScoreT");
    }

    public Vector3 GetPos1()
    {
        return transform.position;

    }




    // Update is called once per frame
    void Update()
    {
        //Debug.Log(Time.deltaTime);
        if (Bomberman != null)
        {
            Balloon.destination = Bomberman.transform.position;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Fire" && !isLose)
        {
            isLose = true;
            Destroy(gameObject.transform.Find("enemy_move").gameObject);
            NavMeshAgent nav = GetComponent<NavMeshAgent>();
            nav.speed = 0;
            Score.GetComponent<ScoreSc>().totalScore += 100;
            Destroy(gameObject, 2.0f);

        }

    }
}
