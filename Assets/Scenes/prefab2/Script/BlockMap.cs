﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMap : MonoBehaviour
{
    public int BlockPAR;

    public int Balloon_KAZU;
    public int Onion_KAZU;
    public int Barrel_KAZU;
    public int Poverty_KAZU;
    public int Chondrion_KAZU;
    public int Monster_KAZU;
    public int Tiger_KAZU;
    public int Pontan_KAZU;

    public GameObject DOOR;

    public int DoorT;

    public GameObject[] Balloon;
    public GameObject[] Onion;
    public GameObject[] Barrel;
    public GameObject[] Poverty;
    public GameObject[] Chondrion;
    public GameObject[] Monster;
    public GameObject[] Tiger;
    public GameObject[] Pontan;


    public int FireP_KAZU;
    public int BombP_KAZU;
    public GameObject[] FireP;
    public GameObject[] BombP;

    public bool Speedon;
    public GameObject SpeedP;
    public int SpeedT;

    public bool Remoteon;
    public GameObject RemoteP;
    public int RemoteT;

    public bool Bombpon;
    public GameObject BombpP;
    public int BombpT;

    public bool Wallpon;
    public GameObject WallpP;
    public int WallpT;

    public bool Firemon;
    public GameObject FiremP;
    public int FiremT;

    public bool Mysteriouson;
    public GameObject MysteriousP;
    public int MysteriousT;




    public GameObject[] Hblockpre;
    public GameObject[] Blockpre;//ブロック配置後の座標を配列でまとめておき、モンスター、アイテムの配置座標に利用したい。（完全ランダムに仕様変更？）


    int[,] Map =
        {
           {1,1,1,1,1,1,1,1,1,1,1},
           {1,0,0,0,0,0,0,0,3,3,1},
           {1,0,1,0,1,0,1,0,1,3,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,1,1,1,1,1,1,1,1,1,1},
        };


    // Start is called before the first frame update
    void Start()
    {
        BlockPAR = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Block_par;

        //ここで敵の配置の決定と数の取り込み、アイテムの配置と数の取り込み、扉の配置を以下で決定する//

        Balloon_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Balloon_suu;
        Onion_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Onion_suu;
        Barrel_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Barrel_suu;
        Poverty_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Poverty_suu;
        Chondrion_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Chondrion_suu;
        Monster_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Monster_suu;
        Tiger_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Tiger_suu;
        Pontan_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Pontan_suu;

        DoorT = 1;

        FireP_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Fire_suu;
        BombP_KAZU = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Bomb_suu;

        Speedon = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Speed_v;
        SpeedT = 1;

        Remoteon = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Remote_v;
        RemoteT = 1;

        Bombpon = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().BombP_v;
        BombpT = 1;

        Wallpon = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().WallP_v;
        WallpT = 1;

        Firemon = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().FireM_v;
        FiremT = 1;

        Mysteriouson = GameObject.FindGameObjectWithTag("Config1").GetComponent<Hikitugitai>().Mysterious_v;
        MysteriousT = 1;


        //ハードブロックとソフトブロックの配置

        for (int point_x = 0; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 1)
                {
                    Instantiate(Hblockpre[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                }
            }
        }

        for (int point_x = 0; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < BlockPAR)//通過パーセントでランダム配置//
                    {
                        Instantiate(Blockpre[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);

                    }
                }
            }
        }
        //扉のランダム配置を記載する(3%の確立で配置、1個配置後、リターンで抜ける)ok
        for (int point_x = 0; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 5.0f && DoorT == 1)//ランダムレンジで１個生成＜5%＞//
                    {
                        Instantiate(DOOR, new Vector3(point_x, 0.5f, point_z), Quaternion.identity);
                        DoorT = DoorT - 1;
                    }
                }
            }
        }


        //Balloon
        for (int point_x = 6; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 15.0f && Balloon_KAZU > 0)//
                    {
                        Instantiate(Balloon[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Balloon_KAZU = Balloon_KAZU - 1;
                    }
                }

            }
        }

        //Onion
        for (int point_x = 8; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 20.0f && Onion_KAZU > 0)//
                    {
                        Instantiate(Onion[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Onion_KAZU = Onion_KAZU - 1;
                    }
                }

            }
        }

        //Barrel
        for (int point_x = 10; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 15.0f && Barrel_KAZU > 0)//ランダムレンジで１個生成＜3%＞//
                    {
                        Instantiate(Barrel[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Barrel_KAZU = Barrel_KAZU - 1;
                    }
                }

            }
        }

        //Poverty
        for (int point_x = 10; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 10.0f && Poverty_KAZU > 0)//
                    {
                        Instantiate(Poverty[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Poverty_KAZU = Poverty_KAZU - 1;
                    }
                }

            }
        }

        //Chondrion
        for (int point_x = 13; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 40.0f && Chondrion_KAZU > 0)//
                    {
                        Instantiate(Chondrion[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Chondrion_KAZU = Chondrion_KAZU - 1;
                    }
                }

            }
        }

        //Monster
        for (int point_x = 13; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 7.0f && Monster_KAZU > 0)//ランダムレンジで１個生成＜3%＞//
                    {
                        Instantiate(Monster[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Monster_KAZU = Monster_KAZU - 1;
                    }
                }

            }
        }

        //Tiger
        for (int point_x = 15; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 25.0f && Tiger_KAZU > 0)//
                    {
                        Instantiate(Tiger[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Tiger_KAZU = Tiger_KAZU - 1;
                    }
                }

            }
        }

        //Pontan
        for (int point_x = 15; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 20.0f && Pontan_KAZU > 0)//
                    {
                        Instantiate(Pontan[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                        Pontan_KAZU = Pontan_KAZU - 1;
                    }
                }

            }
        }

        //FireP
        for (int point_x = 4; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 8.0f && FireP_KAZU > 0)//
                    {
                        Instantiate(FireP[0], new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        FireP_KAZU = FireP_KAZU - 1;
                    }
                }

            }
        }

        //BombP
        for (int point_x = 4; point_x < Map.GetLength(0); point_x++)
        {

            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 8.0f && BombP_KAZU > 0)//
                    {
                        Instantiate(BombP[0], new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        BombP_KAZU = BombP_KAZU - 1;
                    }
                }

            }
        }

        //SpeedP
        for (int point_x = 5; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 10.0f && Speedon == true && SpeedT == 1)
                    {
                        Instantiate(SpeedP, new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        SpeedT = SpeedT - 1;
                    }
                }
            }
        }

        //RemoteP
        for (int point_x = 10; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 7.0f && Remoteon == true && RemoteT == 1)
                    {
                        Instantiate(RemoteP, new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        RemoteT = RemoteT - 1;
                    }
                }
            }
        }

        //BombpP
        for (int point_x = 10; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 5.0f && Bombpon == true && BombpT == 1)
                    {
                        Instantiate(BombpP, new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        BombpT = BombpT - 1;
                    }
                }
            }
        }

        //WallpP
        for (int point_x = 10; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 5.0f && Wallpon == true && WallpT == 1)
                    {
                        Instantiate(WallpP, new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        WallpT = WallpT - 1;
                    }
                }
            }
        }

        //FiremP
        for (int point_x = 10; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 5.0f && Firemon == true && FiremT == 1)
                    {
                        Instantiate(FiremP, new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        FiremT = FiremT - 1;
                    }
                }
            }
        }

        //MysteriousP
        for (int point_x = 0; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 9; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < 100.0f && Mysteriouson == true && MysteriousT == 1)
                    {
                        Instantiate(MysteriousP, new Vector3(point_x, 0.5f, point_z), Quaternion.Euler(0, 180f, 0));
                        MysteriousT = MysteriousT - 1;
                    }
                }
            }
        }

















        // Update is called once per frame
        //void Update()
        {

        }
    }
}
