﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSc : MonoBehaviour
{
    public GameObject Explsionpre;
    public GameObject Bombpre;

    public GameObject Status;
    public StatusSc statuscs;

    public GameObject UnityChan;
    public AudioSource playermove;

    public int Explevel;

    public float kibaku;


    //public PlayerMove playerMove;//スクリプト参照


    //public LayerMask levelMask;

    Ray rayF;
    Ray rayB;
    Ray rayL;
    Ray rayR;
    RaycastHit hitF, hitB, hitL, hitR;

    // Start is called before the first frame update
    void Start()
    {

        kibaku = 1.0f;

        Ray();

        //Explevel = playermove.Explevel;//取り込めてない。
        Explevel = GameObject.Find("unitychan").GetComponent<PlayerMove>().Explevel;

        StartCoroutine("Wait");
    }

    void Update()
    {
        //爆風レベルをステータスの爆風レベルに置き換える？現時点でエラー（ステータスコンポーネントから情報をとれればベスト）
        //Explevel = playermove.Explevel;//取り込めていない

    }

    void Destroy()
    {

    }

    void Ray()
    {
        rayF = new Ray(transform.position, transform.forward);
        rayB = new Ray(transform.position, transform.forward * -1);
        rayL = new Ray(transform.position, transform.right * -1);
        rayR = new Ray(transform.position, transform.right);
    }

    public IEnumerator Wait()
    {
        GameObject Play = GameObject.Find("unitychan");
        Vector3 tmp = Play.transform.position;


        // Debug.Log(kibaku);
        yield return new WaitForSeconds(seconds: 3.5f * kibaku);



        //爆風レベルをステータスの爆風レベルに置き換える？現時点でエラー（ステータスコンポーネントから情報をとれればベスト）


        for (int i = 0; i < Explevel; i++)
        {

            if (Physics.Raycast(rayF, out hitF, i))
            {
                //Debug.LogFormat("hitF.collider.tag = {0} : i = {1}", hitF.collider.tag, i);

                switch (hitF.collider.tag)
                {
                    case "Hblock":
                        break;

                    // case "Fire":
                    //     Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.6f, Mathf.RoundToInt(tmp.z) + i);






                    default:
                        Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.6f, Mathf.RoundToInt(tmp.z) + i);
                        break;
                }

                break;
            }
            else
            {
                //Debug.Log("not hit");
                Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.6f, Mathf.RoundToInt(tmp.z) + i);
            }
        }


        Destroy(gameObject);

        //後方

        for (int i = 0; i < Explevel; i++)
        {

            if (Physics.Raycast(rayB, out hitB, i))
            {

                switch (hitB.collider.tag)
                {
                    case "Hblock":
                        break;

                    default:
                        Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.6f, Mathf.RoundToInt(tmp.z) - i);
                        break;
                }

                break;
            }
            else
            {
                Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.6f, Mathf.RoundToInt(tmp.z) - i);
            }
        }

        Destroy(gameObject);

        //左方

        for (int i = 0; i < Explevel; i++)
        {

            if (Physics.Raycast(rayL, out hitL, i))
            {

                switch (hitL.collider.tag)
                {
                    case "Hblock":
                        break;

                    default:
                        Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x) - i, 0.6f, Mathf.RoundToInt(tmp.z));
                        break;
                }

                break;
            }
            else
            {
                Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x) - i, 0.6f, Mathf.RoundToInt(tmp.z));
            }
        }

        Destroy(gameObject);

        //右方

        for (int i = 0; i < Explevel; i++)
        {

            if (Physics.Raycast(rayR, out hitR, i))
            {

                switch (hitR.collider.tag)
                {
                    case "Hblock":
                        break;

                    default:
                        Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x) + i, 0.6f, Mathf.RoundToInt(tmp.z));
                        break;
                }

                break;
            }
            else
            {
                Instantiate(Explsionpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x) + i, 0.6f, Mathf.RoundToInt(tmp.z));
            }
        }

        Destroy(gameObject);

    }

}