﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using UnityEngine.UI;



public class BonusSc : MonoBehaviour
{
    public GameObject UnityChan;
    public GameObject hidden_bpanel;
    public GameObject hidden_godes;
    public GameObject hidden_coke;
    public GameObject hidden_rokuhachi;
    public GameObject hidden_nakagi;
    public GameObject hidden_oyaji;
    public GameObject Door_o;

    public GameObject Canvas;
    public GameObject Score;
    public GameObject Door_c;

    int lu = 1;
    int ld = 2;
    int rd = 3;
    int ru = 4;
    private string value;
    bool isOpen = false;

    public AudioClip BonusSe;
    public AudioClip ClearSe;
    private AudioSource aud;

    public int enemyZan;

    // Start is called before the first frame update
    void Start()
    {

        this.aud = GetComponent<AudioSource>();
        //Explevel = GameObject.Find("unitychan").GetComponent<PlayerMove>().Explevel;//
        //int enemyZan = GameObject.FindObjectsOfType<Enemy>().Length;
        //Debug.Log(enemyZan);


    }

    // Update is called once per frame
    void Update()
    {
        int enemyZan = GameObject.FindObjectsOfType<Enemy>().Length;
        GetComponent<BonusSc>().enemyZan = enemyZan;
        if (enemyZan == 0 && !isOpen)
        {
            isOpen = true;
            Door_o.transform.localPosition = new Vector3(0, 2, 0);
            //Vector3 tmp = GameObject.Find("Door_c").transform.GetChild(0).gameObject.transform.position;
            //GameObject.Find("Door_o").transform.localPosition = new Vector3(tmp.x, tmp.y + 1.5f, tmp.z);

        }

    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other);
        switch (other.tag)
        {

            case "B_b":

                aud.PlayOneShot(this.BonusSe);
                Score.GetComponent<ScoreSc>().totalScore += 10000;
                Destroy(other.gameObject);
                break;
            case "B_g":

                aud.PlayOneShot(this.BonusSe);
                Score.GetComponent<ScoreSc>().totalScore += 20000;
                Destroy(other.gameObject);
                break;
            case "B_c":

                aud.PlayOneShot(this.BonusSe);
                Score.GetComponent<ScoreSc>().totalScore += 30000;
                Destroy(other.gameObject);
                break;
            case "B_68":

                aud.PlayOneShot(this.BonusSe);
                Score.GetComponent<ScoreSc>().totalScore += 500000;
                Destroy(other.gameObject);
                break;
            case "B_n":

                aud.PlayOneShot(this.BonusSe);
                Score.GetComponent<ScoreSc>().totalScore += 10000000;
                Destroy(other.gameObject);
                break;
            case "B_o":

                aud.PlayOneShot(this.BonusSe);
                Score.GetComponent<ScoreSc>().totalScore += 20000000;
                Destroy(other.gameObject);
                break;
        }
    }
    //GetComponent<BonusSc>().enemyZan != 0)
    //Vector3 tmp = GameObject.FindWithTag("B_b").transform.position;
    //Debug.Log(tmp);
    //GameObject.FindWithTag("B_b").transform.position = new Vector3(tmp.x, tmp.y + 0.5f, tmp.z);
}



