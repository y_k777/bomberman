﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hikitugitai : MonoBehaviour
{
    public int Block_par;

    public int Balloon_suu;
    public int Onion_suu;
    public int Barrel_suu;
    public int Poverty_suu;
    public int Chondrion_suu;
    public int Monster_suu;
    public int Tiger_suu;
    public int Pontan_suu;

    public int Fire_suu;
    public int Bomb_suu;

    public bool Speed_v;
    public bool Remote_v;
    public bool BombP_v;
    public bool WallP_v;
    public bool FireM_v;
    public bool Mysterious_v;

    public GameObject Canvas;

    void Start()
    {

        DontDestroyOnLoad(gameObject);


        //Block_par = int.Parse(GameObject.FindGameObjectWithTag("Block_Par").GetComponent<InputField>().text);

        //Balloon_suu = int.Parse(GameObject.FindGameObjectWithTag("Balloon_Su").GetComponent<InputField>().text);
        //Onion_suu = int.Parse(GameObject.FindGameObjectWithTag("Onion_Su").GetComponent<InputField>().text);
        //Barrel_suu = int.Parse(GameObject.FindGameObjectWithTag("Barrel_Su").GetComponent<InputField>().text);
        //Poverty_suu = int.Parse(GameObject.FindGameObjectWithTag("Poverty_Su").GetComponent<InputField>().text);
        //Chondrion_suu = int.Parse(GameObject.FindGameObjectWithTag("Chondrion_Su").GetComponent<InputField>().text);
        //Monster_suu = int.Parse(GameObject.FindGameObjectWithTag("Monster_Su").GetComponent<InputField>().text);
        //Tiger_suu = int.Parse(GameObject.FindGameObjectWithTag("Tiger_Su").GetComponent<InputField>().text);
        //Pontan_suu = int.Parse(GameObject.FindGameObjectWithTag("Pontan_Su").GetComponent<InputField>().text);

        //Fire_suu = int.Parse(GameObject.FindGameObjectWithTag("Fire_Su").GetComponent<InputField>().text);
        //Bomb_suu = int.Parse(GameObject.FindGameObjectWithTag("Bomb_Su").GetComponent<InputField>().text);

        //Speed_v = GameObject.FindGameObjectWithTag("Speed_C").GetComponent<Toggle>().isOn;
        //Remote_v = GameObject.FindGameObjectWithTag("Remote_C").GetComponent<Toggle>().isOn;
        //BombP_v = GameObject.FindGameObjectWithTag("BombP_C").GetComponent<Toggle>().isOn;
        //WallP_v = GameObject.FindGameObjectWithTag("WallP_C").GetComponent<Toggle>().isOn;
        //FireM_v = GameObject.FindGameObjectWithTag("FireM_C").GetComponent<Toggle>().isOn;
        //Mysterious_v = GameObject.FindGameObjectWithTag("Mysterious_C").GetComponent<Toggle>().isOn;


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Dataikou()
    {
        Block_par = int.Parse(GameObject.FindGameObjectWithTag("Block_Par").GetComponent<InputField>().text);

        Balloon_suu = int.Parse(GameObject.FindGameObjectWithTag("Balloon_Su").GetComponent<InputField>().text);
        Onion_suu = int.Parse(GameObject.FindGameObjectWithTag("Onion_Su").GetComponent<InputField>().text);
        Barrel_suu = int.Parse(GameObject.FindGameObjectWithTag("Barrel_Su").GetComponent<InputField>().text);
        Poverty_suu = int.Parse(GameObject.FindGameObjectWithTag("Poverty_Su").GetComponent<InputField>().text);
        Chondrion_suu = int.Parse(GameObject.FindGameObjectWithTag("Chondrion_Su").GetComponent<InputField>().text);
        Monster_suu = int.Parse(GameObject.FindGameObjectWithTag("Monster_Su").GetComponent<InputField>().text);
        Tiger_suu = int.Parse(GameObject.FindGameObjectWithTag("Tiger_Su").GetComponent<InputField>().text);
        Pontan_suu = int.Parse(GameObject.FindGameObjectWithTag("Pontan_Su").GetComponent<InputField>().text);

        Fire_suu = int.Parse(GameObject.FindGameObjectWithTag("Fire_Su").GetComponent<InputField>().text);
        Bomb_suu = int.Parse(GameObject.FindGameObjectWithTag("Bomb_Su").GetComponent<InputField>().text);

        Speed_v = GameObject.FindGameObjectWithTag("Speed_C").GetComponent<Toggle>().isOn;
        Remote_v = GameObject.FindGameObjectWithTag("Remote_C").GetComponent<Toggle>().isOn;
        BombP_v = GameObject.FindGameObjectWithTag("BombP_C").GetComponent<Toggle>().isOn;
        WallP_v = GameObject.FindGameObjectWithTag("WallP_C").GetComponent<Toggle>().isOn;
        FireM_v = GameObject.FindGameObjectWithTag("FireM_C").GetComponent<Toggle>().isOn;
        Mysterious_v = GameObject.FindGameObjectWithTag("Mysterious_C").GetComponent<Toggle>().isOn;

        TestSceneLoader.NextFealde();

    }

}
