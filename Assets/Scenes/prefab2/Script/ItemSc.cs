﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSc : MonoBehaviour
{
    public GameObject UnityChan;
    public GameObject item_bomb;
    public GameObject item_fire;
    public GameObject item_fireman;
    public GameObject item_speed;
    public GameObject item_wall_pass;
    public GameObject item_bomb_pass;
    public GameObject item_bom_remote;
    public GameObject item_perfectman;

    public GameObject Door;

    public GameObject Score;

    public int Firemutekif;

    public AudioSource aud;

    public AudioClip ItemSe;
    public AudioClip YattaSe;


    //Ray IgnorerRaycast;
    //Ray Blocks;

    // Start is called before the first frame update
    void Start()
    {
        int Firemutekif = 0;
        //int LayerMask = 2;
        //int LayerMask = 9;
        this.aud = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {


    }

    private IEnumerator OnTriggerEnter(Collider other)
    {
        switch (other.tag)
        {
            case "Item_fm":

                aud.PlayOneShot(ItemSe);
                Firemutekif = 1;
                Physics.IgnoreLayerCollision(2, 18);

                Score.GetComponent<ScoreSc>().totalScore += 1000;
                GameObject.Find("fst").transform.position = new Vector3(19f, 0.5f, -2f);
                Destroy(other.gameObject);
                break;
            case "Item_sp":

                aud.PlayOneShot(ItemSe);
                Score.GetComponent<ScoreSc>().totalScore += 1000;
                GameObject.Find("spt").transform.position = new Vector3(7f, 0.5f, -2f);
                Destroy(other.gameObject);
                break;
            case "Item_wpass":

                aud.PlayOneShot(ItemSe);

                Physics.IgnoreLayerCollision(2, 9);

                Score.GetComponent<ScoreSc>().totalScore += 1000;
                GameObject.Find("wst").transform.position = new Vector3(16f, 0.5f, -2f);
                Destroy(other.gameObject);
                break;
            case "Item_bpass":

                aud.PlayOneShot(ItemSe);

                Physics.IgnoreLayerCollision(2, 17);

                Score.GetComponent<ScoreSc>().totalScore += 1000;
                GameObject.Find("bst").transform.position = new Vector3(13f, 0.5f, -2f);
                Destroy(other.gameObject);
                break;
            case "Item_br":

                aud.PlayOneShot(ItemSe);

                GetComponent<PlayerMove>().Remoteflag = 1;

                Score.GetComponent<ScoreSc>().totalScore += 1000;
                GameObject.Find("rct").transform.position = new Vector3(10f, 0.5f, -2f);
                Destroy(other.gameObject);
                break;
            case "Item_pm":

                aud.PlayOneShot(ItemSe);
                Firemutekif = 1;
                Score.GetComponent<ScoreSc>().totalScore += 1000;
                GameObject.Find("pmt").transform.position = new Vector3(22f, 0.5f, -2f);
                //壁抜け、爆弾抜け、爆風耐//
                Physics.IgnoreLayerCollision(2, 9);
                Physics.IgnoreLayerCollision(2, 17);
                Physics.IgnoreLayerCollision(2, 18);

                Destroy(other.gameObject);
                break;

            case "D_open":
                aud.PlayOneShot(YattaSe);
                yield return new WaitForSeconds(2f);
                TestSceneLoader.EndFealde();

                break;
        }

    }
}
