﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pat_bom : MonoBehaviour

{
    public int BombsCount;
    public GameObject Bombpre;
    public AudioSource aud;
    public AudioClip BombsetSe;


    public void BomClick()//爆弾生成<<各値はunitychan参照>>
    {
        BombsCount = GameObject.Find("unitychan").GetComponent<PlayerMove>().BombsCount;

        Vector3 tmp = GameObject.Find("unitychan").transform.position;

        int BombCount = GameObject.FindGameObjectsWithTag("Bombs").Length;

        if (BombsCount > BombCount)//最大BombsCount個置ける。
        {
            aud.PlayOneShot(this.BombsetSe);

            var bomb = Instantiate(Bombpre) as GameObject;
            bomb.name = Bombpre.name;
            bomb.transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.5f, Mathf.RoundToInt(tmp.z));
        }
    }
}
