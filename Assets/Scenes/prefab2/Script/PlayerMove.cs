﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//using System.Linq;

public class PlayerMove : MonoBehaviour
{
    public GameObject Bombpre;
    public GameObject Blockpre;
    public GameObject Explsionpre;
    public GameObject Score;

    public GameObject Canvas;
    public ScoreSc scoresc;
    public GameObject fp;

    //public LayerMask levelMask;

    public int BombsCount;
    public int Explevel;
    //public BombSc bombSc;//スクリプト参照
    // public Foo foo;
    public int Remoteflag;
    public float kibaku = 1.0f;

    public int Firemutekif;

    public AudioSource aud;

    public AudioClip LoseSe;
    public AudioClip ItemSe;
    public AudioClip BombSe;
    public AudioClip BombsetSe;
    public AudioClip YarareSe;




    public float speeddash;

    Ray ray;
    RaycastHit hit;

    public int Bomnosousuu;


    //    [SerializeField] private LineRenderer Laser;
    //    [SerializeField] private GameObject FoucusPoint;



    //GameObject[] Bombs;// クリックしておいたゲームオブジェクトを格納

    //public int ObjectNum;//出現させるオブジェクトの最高値を格納
    //int ObjectWk;//現在のオブジェクト数を格納

    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log(Hikitugitai.name);
        //Hikitugitai.name = "kimijima";
        //Debug.Log(Hikitugitai.name);



        speeddash = 1.0f;

        this.aud = GetComponent<AudioSource>();

        PlayerInputManager.OnUpArrow = MoveForward;
        PlayerInputManager.OnDownArrow = MoveBack;
        PlayerInputManager.OnRightArrow = MoveRight;
        PlayerInputManager.OnLeftArrow = MoveLeft;
        PlayerInputManager.OnSpaceKeyDown = GenerateBomb;
    }

    public Vector3 GetPos()
    {
        return transform.position;

    }

    public void MoveForward()
    {
        transform.Translate(transform.forward * 5.0f * speeddash * Time.deltaTime);
    }

    public void MoveBack()
    {
        transform.Translate(transform.forward * -5.0f * speeddash * Time.deltaTime);
    }

    public void MoveRight()
    {
        transform.Translate(transform.right * 5.0f * speeddash * Time.deltaTime);
    }

    public void MoveLeft()
    {
        transform.Translate(transform.right * -5.0f * speeddash * Time.deltaTime);
    }

    public void GenerateBomb()
    {
        Vector3 tmp = GameObject.Find("unitychan").transform.position;

        int BombCount = GameObject.FindGameObjectsWithTag("Bombs").Length;

        if (BombsCount > BombCount)//最大BombsCount個置ける。
        {
            //Instantiate(Bombpre).transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.5f, Mathf.RoundToInt(tmp.z));
            //設置音//

            aud.PlayOneShot(this.BombsetSe);

            //設置音//
            var bomb = Instantiate(Bombpre) as GameObject;
            bomb.name = Bombpre.name;//クローン表記を外す。
            bomb.transform.position = new Vector3(Mathf.RoundToInt(tmp.x), 0.5f, Mathf.RoundToInt(tmp.z));

            Bomnosousuu = Bomnosousuu + 1;

        }
    }

    // Update is called once per frame
    //void Update()
    //{

    //    //爆弾生成用）//スペースキーでボム置き<<ここに爆弾ボタンを押したら、も追加したい>>←
    //    if (Input.GetKeyDown(KeyCode.Space))
    //    {

    //    }
    //    //リモート爆弾起動（itemscからフラグ1を立てたら、シフトキーを有効にして、起爆時間を身近くするため、シフト押下時、kibaku変数に0.1f代入）
    //    if (Remoteflag == 1)
    //    {
    //        if (Input.GetKeyDown(KeyCode.RightShift))
    //        {
    //            Bombpre.GetComponent<BombSc>().kibaku = 0.1f;//機能していない
    //        }
    //    }
    //}


    private IEnumerator OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Balloon")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }

        else if (collision.gameObject.tag == "tiger")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "poverty")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "pontan")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "onion")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "monster")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "Balloon")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "barrel")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }
        else if (collision.gameObject.tag == "chondrion")
        {
            aud.PlayOneShot(this.LoseSe);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("Gameover");
        }

    }


    private IEnumerator OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Item_b")
        {
            BombsCount += 1;
            //アイテムを取ると1000点＋
            aud.PlayOneShot(this.ItemSe);

            GameObject.Find("bp").GetComponent<bptext>().BombsCount += 1;
            GameObject.Find("bp").GetComponent<bptext>().BPS();


            Score.GetComponent<ScoreSc>().totalScore += 1000;



            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "Item_f")
        {

            Explevel += 1;

            aud.PlayOneShot(this.ItemSe);

            GameObject.Find("fp").GetComponent<fptext>().Explevel += 1;
            GameObject.Find("fp").GetComponent<fptext>().FPS();


            Score.GetComponent<ScoreSc>().totalScore += 1000;

            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "Item_sp")
        {
            speeddash = 2.0f;
        }
        else if (other.gameObject.tag == "Fire")
        {
            if (GetComponent<ItemSc>().Firemutekif == 0)//UNITY実行上は有効、アンドロイド端末に落とすと無効？
            {
                aud.PlayOneShot(this.LoseSe);
                yield return new WaitForSeconds(3f);
                SceneManager.LoadScene("Gameover");
            }

        }
    }
}
