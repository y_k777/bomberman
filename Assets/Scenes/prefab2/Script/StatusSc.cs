﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusSc : MonoBehaviour
{
    public GameObject UnityChan;
    public PlayerMove playermove;
    public ItemSc itemsc;
    public GameObject Bombpre;
    public BombSc bombsc;

    public int Explevel;

    // Start is called before the first frame update
    void Start()
    {
        playermove = UnityChan.GetComponent<PlayerMove>();
        itemsc = UnityChan.GetComponent<ItemSc>();
        bombsc = Bombpre.GetComponent<BombSc>();

    }

    // Update is called once per frame
    void Update()
    {
        Explevel = playermove.Explevel;
    }
}
