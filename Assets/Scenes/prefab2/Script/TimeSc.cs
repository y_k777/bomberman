﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeSc : MonoBehaviour
{
    GameObject TimerText;
    public float time = 150f;

    public AudioClip Time100Se;
    public AudioClip Time0Se;
    public AudioClip main;
    public AudioClip Isoide;
    public AudioClip PinchiSe;
    public AudioClip DamedaSe;


    public AudioSource audBGM1;
    public AudioSource audBGM2;
    public AudioSource audSE;
    int i;
    public GameObject[] Blockpre;
    public int BlockPAR;

    //public float timeOut;
    //public float timeElapsed;


    int[,] Map =
    {
           {1,1,1,1,1,1,1,1,1,1,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,0,1,0,1,0,1,0,1,0,1},
           {1,0,0,0,0,0,0,0,0,0,1},
           {1,1,1,1,1,1,1,1,1,1,1},
        };

    // Start is called before the first frame update
    void Start()
    {
        //this.audSE = GetComponent<AudioSource>();
        this.TimerText = GameObject.Find("Time");
        i = 1;
        //audSE.PlayOneShot(this.main);
        //timeOut = 0.5f;
        PlayerInputManager.OnUpArrow += Test;
    }

    void Test()
    {
        Debug.Log("よばれた");
    }

    // Update is called once per frame
    void Update()
    {
        this.time -= Time.deltaTime;
        this.TimerText.GetComponent<Text>().text = "TIME:" + this.time.ToString("F0");




        if (this.time < 100f && i == 1)
        {
            this.TimerText.GetComponent<Text>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            audSE.PlayOneShot(this.Time100Se);
            i = i - 1;
        }

        if (this.time < 50f && i == 0)
        {
            //メイン音を止めて、残り50秒音に切り替えたい//
            this.TimerText.GetComponent<Text>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            audBGM1.Stop();
            audBGM2.Play();
            audSE.PlayOneShot(this.PinchiSe);
            //audSE.PlayOneShot(this.Isoide);
            i = i - 1;
        }

        if (this.time < 1f && i == -1)
        {
            audBGM2.Stop();
            audSE.PlayOneShot(this.Time0Se);
            Debug.Log("gameover");
            this.TimerText.GetComponent<Text>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
            i = i - 1;
            audBGM2.Stop();

        }

        if (this.time <= 0f)
        {
            this.TimerText.GetComponent<Text>().text = "TIME OVER";

            //ソフトブロックで埋め尽くす／／(埋め尽くす様を画面に表示させたいコンマ何秒で一個配置、タイム0音鳴り終わりでゲームオーバーシーンに行きたい)
            BlockPAR = 100;

            StartCoroutine(DelayBlock());
            StartCoroutine(End());//コルーチンが効かない

            Debug.Log("end");
            //Invoke使えない？（上記の様子を見せてから画面移行させたい）
        }
    }

    IEnumerator End()
    {
        //Debug.Log("start");
        yield return new WaitForSeconds(6.0f);
        SceneManager.LoadScene("Gameover");
    }

    IEnumerator DelayBlock()
    {
        for (int point_x = 0; point_x < Map.GetLength(0); point_x++)
        {
            for (int point_z = 0; point_z < Map.GetLength(1); point_z++)
            {
                if (Map[point_x, point_z] == 0)
                {
                    if (Random.Range(0.0f, 100.0f) < BlockPAR)
                    {

                        //timeElapsed += Time.deltaTime;
                        //if (timeElapsed >= timeOut)
                        {
                            Instantiate(Blockpre[0], new Vector3(point_x, 1.0f, point_z), Quaternion.identity);
                            yield return new WaitForSeconds(0.005f);
                            //Invoke使えない？
                        }
                    }
                }
            }
        }
    }

}

